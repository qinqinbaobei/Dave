
        /**
         * @api {post} /userinfo2/add userinfo2配置新增
         * @apiDescription ""
         * @apiName Addbanner
         * @apiGroup userinfo2配置
         
                     * @apiParam {string} name 
                 
                     * @apiParam {string} password 
                 
                     * @apiParam {string} account 
                 
                     * @apiParam {string} isdelete 
                 
                     * @apiParam {string} forbidden 
                 
                     * @apiParam {string} rank 
                 
                     * @apiParam {string} createdate 
                 
                     * @apiParam {string} updatedate 
                 
         * @apiSuccess {json} code
         * @apiSampleRequest http://gtrhtr.51vip.biz/banner/add
         * @apiVersion 0.0.0
        */
       

             
		/**
		 * @api {get} /userinfo2/query userinfo2配置查询详情
		 * @apiDescription ""
		 * @apiName Getbanner
		 * @apiGroup userinfo2配置
          
                * @apiParam {string} name 
                 
                * @apiParam {string} password 
                 
                * @apiParam {string} account 
                 
                * @apiParam {string} isdelete 
                 
                * @apiParam {string} forbidden 
                 
                * @apiParam {string} rank 
                 
                * @apiParam {string} createdate 
                 
                * @apiParam {string} updatedate 
                
		 * @apiSuccess {json} code
		 * @apiSampleRequest http://gtrhtr.51vip.biz/banner/query
		 * @apiVersion 0.0.0
		 */

         
       /**
        * @api {get} /userinfo2/update userinfo2配置修改
        * @apiDescription ""
        * @apiName Updatefinancial
        * @apiGroup userinfo2配置
         
                * @apiParam {string} name 
                 
                * @apiParam {string} password 
                 
                * @apiParam {string} account 
                 
                * @apiParam {string} isdelete 
                 
                * @apiParam {string} forbidden 
                 
                * @apiParam {string} rank 
                 
                * @apiParam {string} createdate 
                 
                * @apiParam {string} updatedate 
                
       
        * @apiSuccess {json} code
        * @apiSampleRequest http://gtrhtr.51vip.biz/financial/update
        * @apiVersion 0.0.0
       */

       
		/**
		 * @api {get} /userinfo2/delete userinfo2配置删除
		 * @apiDescription ""
		 * @apiName Deletebanner
		 * @apiGroup userinfo2配置
         
                 * @apiParam {string} name 
                 
                 * @apiParam {string} password 
                 
                 * @apiParam {string} account 
                 
                 * @apiParam {string} isdelete 
                 
                 * @apiParam {string} forbidden 
                 
                 * @apiParam {string} rank 
                 
                 * @apiParam {string} createdate 
                 
                 * @apiParam {string} updatedate 
                 
		 * @apiSuccess {json} code
		 * @apiSampleRequest http://gtrhtr.51vip.biz/banner/delete
		 * @apiVersion 0.0.0
		 */
    

        /**
         * @api {post} /financial/add financial配置新增
         * @apiDescription ""
         * @apiName Addbanner
         * @apiGroup financial配置
         
                     * @apiParam {string} price 字段描述
                 
                     * @apiParam {string} oldId 
                 
                     * @apiParam {string} userid 
                 
                     * @apiParam {string} type 
                 
                     * @apiParam {string} actualDistribution 
                 
                     * @apiParam {string} name 
                 
                     * @apiParam {string} isdelete 
                 
                     * @apiParam {string} createdate 
                 
                     * @apiParam {string} updatedate 
                 
         * @apiSuccess {json} code
         * @apiSampleRequest http://gtrhtr.51vip.biz/banner/add
         * @apiVersion 0.0.0
        */
       

             
		/**
		 * @api {get} /financial/query financial配置查询详情
		 * @apiDescription ""
		 * @apiName Getbanner
		 * @apiGroup financial配置
          
                * @apiParam {string} price 字段描述
                 
                * @apiParam {string} oldId 
                 
                * @apiParam {string} userid 
                 
                * @apiParam {string} type 
                 
                * @apiParam {string} actualDistribution 
                 
                * @apiParam {string} name 
                 
                * @apiParam {string} isdelete 
                 
                * @apiParam {string} createdate 
                 
                * @apiParam {string} updatedate 
                
		 * @apiSuccess {json} code
		 * @apiSampleRequest http://gtrhtr.51vip.biz/banner/query
		 * @apiVersion 0.0.0
		 */

         
       /**
        * @api {get} /financial/update financial配置修改
        * @apiDescription ""
        * @apiName Updatefinancial
        * @apiGroup financial配置
         
                * @apiParam {string} price 字段描述
                 
                * @apiParam {string} oldId 
                 
                * @apiParam {string} userid 
                 
                * @apiParam {string} type 
                 
                * @apiParam {string} actualDistribution 
                 
                * @apiParam {string} name 
                 
                * @apiParam {string} isdelete 
                 
                * @apiParam {string} createdate 
                 
                * @apiParam {string} updatedate 
                
       
        * @apiSuccess {json} code
        * @apiSampleRequest http://gtrhtr.51vip.biz/financial/update
        * @apiVersion 0.0.0
       */

       
		/**
		 * @api {get} /financial/delete financial配置删除
		 * @apiDescription ""
		 * @apiName Deletebanner
		 * @apiGroup financial配置
         
                 * @apiParam {string} price 字段描述
                 
                 * @apiParam {string} oldId 
                 
                 * @apiParam {string} userid 
                 
                 * @apiParam {string} type 
                 
                 * @apiParam {string} actualDistribution 
                 
                 * @apiParam {string} name 
                 
                 * @apiParam {string} isdelete 
                 
                 * @apiParam {string} createdate 
                 
                 * @apiParam {string} updatedate 
                 
		 * @apiSuccess {json} code
		 * @apiSampleRequest http://gtrhtr.51vip.biz/banner/delete
		 * @apiVersion 0.0.0
		 */
    